#pragma once

class Vaixell
{
public:
	Vaixell() {};
	Vaixell(int fila, int columna, int direccio, int nCaselles);
	void setFila(int fila) { m_fila = fila; };
	void setColumna(int columna) { m_columna = columna; };
	void setDireccio(int direccio) { m_direcci� = direccio; };
	void setnCaselles(int n_caselles) { m_nCaselles = n_caselles; };
	int registraAtac(int fila, int columna);
private:
	static const int MAX_CASELLES = 4;
	int m_fila;
	int m_columna;
	int m_direcci�;
	bool m_estat[MAX_CASELLES];	//inicializar las casillas a true
	int m_nCaselles;
	int m_nCasellesVives;
};
