#pragma once
#include "Jugador.h"
#include "Pantalla.h"

class Partida
{
public:
	Partida() {};
	~Partida() {};
	void inicialitza(const string& fitxer1, const string& fitxer2);
	void setTipusJugador(int tipus);
	void visualitza() { m_tauler.visualitza(); };
	bool fesMoviment(int x, int y, int& fila, int&columna, int& tauler);
	bool finalPartida();
private:
	Pantalla m_tauler;
	Jugador m_jugador1;
	Jugador m_jugador2;
	int m_torn;
};
