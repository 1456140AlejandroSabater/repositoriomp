#pragma once
#include "Vaixell.h"


#define N_FILES 10
#define N_COLUMNES 10

class Jugador
{
public:
	Jugador() {};
	~Jugador() {};
	void setTipus(int num);
	void afegeixVaixell(int fila, int columna, int direccio, int nCaselles);
	int getNVaixellsVius() const;
	int consultaAtacEnemic(int fila, int columna);
	void registraResultatAtac(int fila, int columna, int resultat);
	void generaPosicioAtac(int& fila, int& columna);
private:
	static const int N_VAIXELLS = 10;
	Vaixell m_llistaVaixells[N_VAIXELLS];
	int m_nVaixellsVius=0;
	bool m_tipusJugador;
	int m_taulerContrari[N_FILES][N_COLUMNES];
};