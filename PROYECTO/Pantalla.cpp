
#include "Pantalla.h"


void Pantalla::incialitza()
{
	for (int i = 0; i < N_FILES; i++)
	{
		for (int j = 0; j < N_COLUMNES; j++)
		{
			m_taulerPropi[i][j] = 0;
			m_taulerContrari[i][j] = 0;
		}
	}

	m_aigua.create("data/aigua.png");
	m_tauler.create("data/caselles.png");
	m_vaixell.create("data/vaixell.png");
	m_tocat.create("data/tocat.png");
	m_enfonsat.create("data/enfonsat.png");
}

void Pantalla::visualitza()
{
	m_tauler.draw(0, 0);
	m_tauler.draw(600, 0);	//Tauler contrari

	for (int i = 0; i < N_FILES; i++)
	{
		for (int j = 0; j < N_COLUMNES; j++)
		{
			if (m_taulerPropi[i][j] == 1)
			{
				m_vaixell.draw(52*j + INI_PANTALLA_X, INI_PANTALLA_Y + i* 52);	//Pintar tauler propi
			}

			/*
			if (m_taulerContrari[i][j] == 1)
			{
				m_vaixell.draw(600+(52 * j + INI_PANTALLA_X), INI_PANTALLA_Y + i * 52);
			}
			*/
		}
	}
}

void Pantalla::getPosicio(int x, int y, int& fila, int&columna, int& tauler)
{
	fila = x / MIDA_CASELLA;
	columna = y / MIDA_CASELLA;
	tauler = 1;
}

void Pantalla::setPosicioPropi(int fila, int columna, int estat)
{
	m_taulerPropi[fila][columna] = estat;
}

void Pantalla::setPosicioContrari(int fila, int columna, int estat)
{
	m_taulerContrari[fila][columna] = estat;
}

int Pantalla::getValorTaulell(int fila, int columna, int taulell)
{
	if (taulell == 1)
	{
		return m_taulerPropi[fila][columna];
	}
	else
	{
		return m_taulerContrari[fila][columna];
	}
}