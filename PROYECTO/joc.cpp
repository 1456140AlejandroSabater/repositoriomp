#include "joc.h"

#include "Partida.h" //
#include <iostream>
using namespace std;
//-----------------------------------------
// joc: porta el control de tot el joc
//-----------------------------------------
void joc()
{

	int x, y;
	int fila = 0;
	int columna = 0;
	int tauler = 0;
	//Inicialitza un objecte de la classe Screen que s'utilitza per gestionar la finestra grafica
	Screen pantalla(MIDA_X, MIDA_Y);

	//Mostrem la finestra grafica
	pantalla.show();

	Partida partida;	//Inicialitzem la partida

	string fitxerA = "data/vaixells_jugador_huma.txt";		//Noms dels fitxers
	string fitxerB = "data/vaixells_jugador_ordinador.txt";

	//Inicialitzem la partida
	partida.inicialitza(fitxerA, fitxerB);
	partida.setTipusJugador(1);
	partida.setTipusJugador(2);
	// ********************************************
	// AFEGIR CODI: Crear tots els grafics del joc
	//				Inicialitzar variables posicio vaixell
	// ********************************************


	do
	{

		// Captura tots els events de ratol� i teclat de l'ultim cicle
		pantalla.processEvents();

		//Visualitza
		partida.visualitza();


		// *****************************************************************************
		// AFEGIR CODI:	Capturar si s'ha fet clic amb el ratol�
		//				Si s'ha fet clic amb el ratol� modificar posici� (fila i columna del tauler) 
		//				del vaixell en funci� de la posici� del ratol�
		// *****************************************************************************




		if (Mouse_getButLeft())
		{
			x = Mouse_getX();	//Agafem la posicio x i y del ratol�
			y = Mouse_getY();

			if (partida.fesMoviment(x, y, fila, columna, tauler) == true)
			{
				//Aqu� hem de ficar l'array del jugador a 1, i pintar
			}

			cout << "Posicio capturada en el tauler real: (" << fila + 1 << "," << columna + 1 << ")" << endl;

		}


		//vaixell.draw(INI_PANTALLA_X + fila*MIDA_CASELLA, INI_PANTALLA_Y + columna*MIDA_CASELLA);


		// *****************************************************************************
		// AFEGIR CODI: Redibuixar tauler 
		//				Dibuixar el vaixell a la posici� (fila i columna) que toqui						
		// *****************************************************************************

		// Actualitza la pantalla
		pantalla.update();

	} while (!Keyboard_GetKeyTrg(KEYBOARD_ESCAPE));
	// Sortim del bucle si pressionem ESC

}
