#include "Partida.h"
#include "Pantalla.h"

#include <iostream>
#include <fstream>

void Partida::inicialitza(const string& fitxer1, const string& fitxer2)
{
	m_tauler.incialitza();	//inicialitzem els sprites	

	int fila, columna, posiciones, orien;


	//LECTURA DEL FITXER 
	ifstream fitxerA;
	fitxerA.open(fitxer1);

	if (fitxerA.is_open())
	{
		

		while (!fitxerA.eof())
		{

			fitxerA >> fila >> columna >> posiciones >> orien;
			fila = fila - 1;
			columna = columna - 1;
			if (orien == 0)
			{
				for (int i = columna; i < (posiciones + columna); i++)
				{
					m_tauler.setPosicioPropi(fila, i, 1); //POSEM A 1 LA CASELLA CORRESPONENT A LA MATRIU
					m_jugador1.afegeixVaixell(fila, columna, 0, posiciones);
				}
			}
			else
			{
				for (int i = fila; i < (posiciones + fila); i++)
				{
					m_tauler.setPosicioPropi(i, columna, 1);	//POSEM A 1 LA CASELLA CORRESPONENT A LA MATRIU
					m_jugador1.afegeixVaixell(fila, columna, 1, posiciones);
				}
			}
		}
		fitxerA.close();
	}




	//LECTURA DEL FITXER DEL CONTRARI
	ifstream fitxerB;
	fitxerB.open(fitxer2);

	if (fitxerB.is_open())
	{
		
		while (!fitxerB.eof())
		{

			fitxerB >> fila >> columna >> posiciones >> orien;
			fila = fila - 1;
			columna = columna - 1;

			if (orien == 0)
			{
				for (int i = columna; i < (posiciones + columna); i++)
				{
					m_tauler.setPosicioContrari(fila, i, 1);	//POSEM A 1 LA CASELLA CORRESPONENT A LA MATRIU
					m_jugador2.afegeixVaixell(fila, columna, 0, posiciones);
				}
			}
			else
			{
				for (int i = fila; i < (posiciones + fila); i++)
				{
					m_tauler.setPosicioContrari(i, columna, 1);	//POSEM A 1 LA CASELLA CORRESPONENT A LA MATRIU
					m_jugador2.afegeixVaixell(fila, columna, 1, posiciones);
				}
			}
		}
		fitxerB.close();
	}

	ofstream fitxerC;
	fitxerC.open("data/tauler_huma.txt");

	for (int i = 0; i < N_FILES; i++)
	{
		for (int j = 0; j < N_COLUMNES; j++)
		{
			fitxerC << m_tauler.getValorTaulell(i, j, 1);
		}
		fitxerC << '\n';
	}
	fitxerC.close();

	ofstream fitxerD;
	fitxerD.open("data/tauler_ordinador.txt");

	for (int i = 0; i < N_FILES; i++)
	{
		for (int j = 0; j < N_COLUMNES; j++)
		{
			fitxerD << m_tauler.getValorTaulell(i, j, 2);
		}
		fitxerD << '\n';
	}
	fitxerD.close();


}

bool Partida::fesMoviment(int x, int y, int& fila, int&columna, int& tauler)	//�Perque retornar el tauler?
{
	m_tauler.getPosicio(x, y, fila, columna, tauler);
	if (fila < 10)
	{
		if (columna < 10)
		{
			return true;
		}
	}
	return false;
}

void Partida::setTipusJugador(int tipus)
{
	m_jugador1.setTipus(tipus);
	m_jugador2.setTipus(tipus);
}