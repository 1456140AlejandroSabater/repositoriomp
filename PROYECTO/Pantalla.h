#pragma once

#include <iostream>
#include "lib/libreria.h"

#define N_FILES 10
#define N_COLUMNES 10

///////////////////////////////////////////////////////////////////////////

//========================================================================
// Mida Pantalla
const int MIDA_X = 1120;
const int MIDA_Y = 520;

//========================================================================
// Inici del tauler respecte la cantonada superior esquerre
const int INI_PANTALLA_X = 0;
const int INI_PANTALLA_Y = 0;

//========================================================================
// Fi del tauler respecte la cantonada inferior dreta
const int FI_PANTALLA_X = 520;
const int FI_PANTALLA_Y = 520;

// Mida d'una casella del tauler
const int MIDA_CASELLA = 52;

using namespace std;


class Pantalla
{
public:
	Pantalla() {};
	~Pantalla() {};
	void incialitza(); // Aquest m�tode es per inicialitzar els sprites
	void visualitza();
	void setPosicioPropi(int fila, int columna, int estat);
	void setPosicioContrari(int fila, int columna, int estat);
	int getValorTaulell(int fila, int columna, int taulell);
	void getPosicio(int x, int y, int& fila, int&columna, int& tauler);
private:
	int m_taulerPropi[N_FILES][N_COLUMNES];
	int m_taulerContrari[N_FILES][N_COLUMNES];
	Sprite m_tauler;
	Sprite m_vaixell;
	Sprite m_aigua;
	Sprite m_tocat;
	Sprite m_enfonsat;
};
